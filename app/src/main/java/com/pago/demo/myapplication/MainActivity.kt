package com.pago.demo.myapplication

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.pago.demo.myapplication.service.AccountService
import com.pago.demo.myapplication.ui.account.AccountFragment
import com.pago.demo.myapplication.ui.registration.RegistrationFragment
import com.pago.demo.myapplication.ui.transactions.TransactionsPagerFragment
import com.pago.demo.myapplication.util.ParameterManager
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.Security


class MainActivity : AppCompatActivity() {

    var mInstance: MainActivity? = null;
    private var mTransactionFragment = TransactionsPagerFragment();
    private var mAccountService = AccountService()
    private var mRegistrationFragment = RegistrationFragment(mAccountService)
    private var mAccountFragment =
        AccountFragment(mAccountService)

    @Synchronized
    fun getInstance(): MainActivity? {
        return mInstance
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.mInstance = this
        Security.removeProvider("BC");
        Security.insertProviderAt(BouncyCastleProvider(), 1)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById<View>(R.id.topAppBar) as Toolbar
        setSupportActionBar(toolbar)

        openTransactionsFragment()
    }

    // Menu icons are inflated just as they were with actionbar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.top_app_bar, menu)
        return true
    }

    private fun emptyFrameLayout() {
        val fragmentManagerTransaction = supportFragmentManager.beginTransaction()
        fragmentManagerTransaction.remove(mTransactionFragment)
        fragmentManagerTransaction.remove(mRegistrationFragment)
        fragmentManagerTransaction.remove(mAccountFragment)
        fragmentManagerTransaction.commit()
    }

    private fun loadFragmentToFrame(fragment: Fragment) {
        val fragmentManagerTransaction = supportFragmentManager.beginTransaction()
        fragmentManagerTransaction.add(R.id.mainContentFrame, fragment)
        fragmentManagerTransaction.commit()
    }

    public fun openTransactionsFragment() {
        emptyFrameLayout()
        loadFragmentToFrame(mTransactionFragment)
    }

    public fun openAccountFragment(mi: MenuItem) {
        emptyFrameLayout()
        val pagoAlias = ParameterManager.getParameter(this, "PAGO_ALIAS")
        val mnemonic = ParameterManager.getParameter(this, "ALGORAND_ACCOUNT_MNEMONIC")
        if ( pagoAlias == null || mnemonic == null) {
            loadFragmentToFrame(mRegistrationFragment)
        } else {
            loadFragmentToFrame(mAccountFragment)
        }
    }
    override fun onBackPressed() {
        openTransactionsFragment()
    }
}
