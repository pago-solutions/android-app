package com.pago.demo.myapplication.ui.transactions.model

import com.pago.demo.myapplication.model.TransactionEnvelope
import com.pago.demo.myapplication.model.TransactionInterfaceResponse

data class TransactionItem(
    var text1: String,
    val text2: String,
    val text3: String,
    var transactionEnvelope: TransactionEnvelope,
    var transactionInterfaceResponse: TransactionInterfaceResponse
)
