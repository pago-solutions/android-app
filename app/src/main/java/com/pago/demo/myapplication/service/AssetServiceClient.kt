package com.pago.demo.myapplication.service

import com.pago.demo.myapplication.config.TransactionGatewayConfig
import com.pago.demo.myapplication.model.Asset
import com.pago.demo.myapplication.model.Page
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*


interface AssetServiceClient {
    @GET("/asset/list")
    fun listAssets(@Query("page_size") page_size: Int?): Call<Page<Asset>>;
}

class AssetServiceClientImpl {
    companion object {
        var transactionGatewayConfig: TransactionGatewayConfig = TransactionGatewayConfig()
        fun getAssetServiceClient(): AssetServiceClient {
            return Retrofit.Builder()
                .baseUrl(transactionGatewayConfig.url)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(AssetServiceClient::class.java)
        }
    }
}
