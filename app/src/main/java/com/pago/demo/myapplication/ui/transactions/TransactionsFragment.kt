package com.pago.demo.myapplication.ui.transactions

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.pago.demo.myapplication.R
import com.pago.demo.myapplication.model.*
import com.pago.demo.myapplication.service.AssetService
import com.pago.demo.myapplication.service.Events
import com.pago.demo.myapplication.service.TransactionService
import com.pago.demo.myapplication.ui.transactions.model.TransactionItem
import com.pago.demo.myapplication.util.ParameterManager
import com.squareup.okhttp.internal.Internal.logger
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Level


/**
 * A placeholder fragment containing a simple view.
 */
class TransactionsFragment(
    private var transactionService: TransactionService,
    private var assetService: AssetService = AssetService(),
    private var pagePosition: Int
) : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private var fragmentRoot: View? = null
    private var recyclerView: RecyclerView? = null
    private var transactionsViewAdapter: TransactionsViewAdapter? = null
    private var transactionItemList: ArrayList<TransactionItem>? = ArrayList<TransactionItem>()
    private var swipeToRefresh: SwipeRefreshLayout? = null
    private var assets: List<Asset> = ArrayList(10)

    var dateFormatter = SimpleDateFormat("EEE, MMM d")

    val updateAssets: (List<Asset>?, Throwable?) -> Unit = { assetList, throwable ->
        if (throwable == null) {
            if (assetList != null) {
                assets = assetList
            }
        }
        buildTransactionList()
    }

    val transferTransactionToTransactionInterface = { transactionEnvelope: TransactionEnvelope, transactionInterface: TransferTransactionResponse ->
        val assetId = transactionInterface.assetId
        val asset: Asset? = assets.find {
            it.assetId == assetId
        }
        var amount = BigDecimal(transactionInterface.amount, 6)
        if (asset != null) {
            amount = BigDecimal(transactionInterface.amount, asset!!.decimals)
        }
        val df = DecimalFormat()
        df.minimumFractionDigits = 0
        df.isGroupingUsed = true;
        val transactionAmount = df.format(
            amount.stripTrailingZeros()
        )
        TransactionItem(
            transactionInterface.receiver!!.substringBefore("$"),
            dateFormatter.format(transactionEnvelope.createDate),
            "$transactionAmount ${asset?.unitName ?: "ALGO"}",
            transactionEnvelope,
            transactionInterface
        )
    }

    val updateTransactions: (List<TransactionEnvelope>?, Throwable?) -> Unit = { transactionEnvelopeList, throwable ->
        if (transactionEnvelopeList != null) {
            transactionItemList!!.clear()

            transactionEnvelopeList.forEach { transactionEnvelope: TransactionEnvelope ->
                if (transactionEnvelope.transactionInterfaceResponse is GroupTransactionResponse) {
                    var transactionInterface = (transactionEnvelope!!.transactionInterfaceResponse as GroupTransactionResponse?)!!
                    transactionInterface.transactionInterfaceMapResponse!!.values.forEach{
                        val pagoAlias = ParameterManager.getParameter(context, "PAGO_ALIAS")
                        if ((it as TransferTransactionResponse).sender == pagoAlias) {
                            transactionItemList!!.add(
                                transferTransactionToTransactionInterface(transactionEnvelope, it as TransferTransactionResponse)
                            )
                        }
                    }
                } else {
                    transactionItemList!!.add(
                        transferTransactionToTransactionInterface(transactionEnvelope, transactionEnvelope.transactionInterfaceResponse!! as TransferTransactionResponse)
                    )
                }
                transactionsViewAdapter!!.notifyDataSetChanged()
            }
        } else {
            logger.log(Level.FINER, throwable!!.message)
            val builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
            builder.setMessage("Could not retrieve transactions. Please try again later")
                .setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                    // Handle Ok
                }).create()
        }
    }

    fun buildRefreshListener() {
        swipeToRefresh = fragmentRoot!!.findViewById<SwipeRefreshLayout>(R.id.swipeToRefresh)
        swipeToRefresh!!.setOnRefreshListener(this)
    }

    override fun onRefresh() {
        swipeToRefresh!!.setRefreshing(true)
        assetService.listAssets(updateAssets)
        swipeToRefresh!!.setRefreshing(false)
    }

    fun buildTransactionList() {

        var pagoAlias: String? = ParameterManager.getParameter(context, "PAGO_ALIAS")
        if (pagoAlias != null ) {
            if (pagePosition == 0) {
                transactionService.getTransactionsForSender(pagoAlias!!, "RECEIVED", updateTransactions)
            } else {
                transactionService.getTransactionsForSender(pagoAlias!!, "COMMITTED", updateTransactions)
            }
        }
    }

    fun buildRecyclerView() {
        recyclerView = fragmentRoot!!.findViewById(R.id.recyclerView)
        recyclerView!!.setHasFixedSize(true)
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(fragmentRoot!!.context)
        transactionsViewAdapter =
            TransactionsViewAdapter(
                transactionItemList!!
            )
        recyclerView!!.setLayoutManager(layoutManager)
        recyclerView!!.setAdapter(transactionsViewAdapter)
        onRefresh()
        if (pagePosition == 0) {
            var accountMnemonic: String? = ParameterManager.getParameter(context!!, "ALGORAND_ACCOUNT_MNEMONIC")
            transactionsViewAdapter!!.setOnItemClickListener(object : TransactionsViewAdapter.OnItemClickListener {
                override fun onTransactionSignClick(position: Int) {
                    if (accountMnemonic != null ) {
                        val removeSignedItem: (String) -> Unit = {
                            if (it == "Success") {
                                onRefresh()
                            } else {
                                val builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
                                builder.setMessage("Could not submit signed transaction. Please try again later")
                                    .setPositiveButton("Ok", DialogInterface.OnClickListener { _, _ ->
                                    }).create()
                            }
                        }
                        transactionService.signTransaction(
                            accountMnemonic,
                            transactionItemList!!.get(position).transactionEnvelope!!,
                            transactionItemList!!.get(position).transactionInterfaceResponse!! as TransferTransactionResponse,
                            removeSignedItem
                        )
                    }
                }
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentRoot = inflater.inflate(R.layout.transactions_fragment, container, false)
        buildRefreshListener()
        buildRecyclerView()
        return fragmentRoot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Events.serviceEvent.observe(this, Observer {
            onRefresh()
        })
    }
}