package com.pago.demo.myapplication.service

import com.algorand.algosdk.account.Account
import com.pago.demo.myapplication.model.AccountRegistrationRequest
import com.pago.demo.myapplication.model.AccountRegistrationResponse
import com.squareup.okhttp.internal.Internal.logger
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.logging.Level

class AccountService {

    var accountServiceClient: AccountServiceClient = AccountServiceClientImpl.getAccountServiceClient();

    fun createAccount (
        accountRegistrationRequest: AccountRegistrationRequest?,
        callback: (AccountRegistrationResponse?, Throwable?) -> Unit
    ) {
        accountServiceClient.createRegistration(accountRegistrationRequest).enqueue(object :
            Callback<AccountRegistrationResponse> {
            override fun onFailure(call: Call<AccountRegistrationResponse>?, t: Throwable?) {
                callback(null, t)
            }

            override fun onResponse(call: Call<AccountRegistrationResponse>?, response: Response<AccountRegistrationResponse>?) {
                if (response!!.isSuccessful) {
                    callback(response!!.body(), null)
                } else {
                    callback(null, Throwable(response.errorBody().string()))
                }
            }
        })
    }

    fun buildAccountRegistration(
        accountMnemonic: String,
        pagoAlias: String,
        emailAddress: String,
        notificationEndpoint: String?
    ): AccountRegistrationRequest? {
        var algorandAccount: Account? = null
        try {
            algorandAccount = Account(accountMnemonic)
        } catch (e: IllegalArgumentException) {
            logger.log(Level.FINER, e.message)
            return null
        }

        return AccountRegistrationRequest(
            pagoAlias,
            algorandAccount.address.toString(),
            notificationEndpoint,
            emailAddress,
            "PERSONAL"
        )
    }
}