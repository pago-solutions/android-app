package com.pago.demo.myapplication.service

import com.pago.demo.myapplication.model.Asset
import com.pago.demo.myapplication.model.Page
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AssetService {
    var assetServiceClient: AssetServiceClient = AssetServiceClientImpl.getAssetServiceClient()

    fun listAssets(
        callback: (List<Asset>?, Throwable?) -> Unit
    ) {
        assetServiceClient.listAssets(100).enqueue(object :
            Callback<Page<Asset>> {
            override fun onFailure(call: Call<Page<Asset>>?, t: Throwable?) {
                callback(null, t)
            }

            override fun onResponse(call: Call<Page<Asset>>?, response: Response<Page<Asset>>?) {
                var assets = response!!.body().contents
                callback(assets, null)
            }
        })
    }
}

