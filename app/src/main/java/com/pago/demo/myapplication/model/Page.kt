package com.pago.demo.myapplication.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Page<T>(
    @JsonProperty("contents")
    var contents: List<T>?,
    @JsonProperty("total")
    var total: Int?,
    @JsonProperty("page_number")
    var page_number: Int?,
    @JsonProperty("page_size")
    var page_size: Int?
)
