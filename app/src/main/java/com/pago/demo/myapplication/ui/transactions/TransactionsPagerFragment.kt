package com.pago.demo.myapplication.ui.transactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.pago.demo.myapplication.R

class TransactionsPagerFragment : Fragment() {

    private var fragmentRoot: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentRoot = inflater.inflate(R.layout.transaction_pages_fragment, container, false)
        val transactionsPagerAdapter = TransactionsPagerAdapter(this.context!!, childFragmentManager)
        val transactionPager: ViewPager = fragmentRoot!!.findViewById(R.id.transactionPager)
        transactionPager.adapter = transactionsPagerAdapter
        val tabs: TabLayout = fragmentRoot!!.findViewById(R.id.transactionPagerTabs)
        tabs.setupWithViewPager(transactionPager)
        return fragmentRoot
    }
}