package com.pago.demo.myapplication.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class AccountRegistrationResponse (
    @JsonProperty("id")
    var id: UUID?,
    @JsonProperty("alias")
    var alias : String?,
    @JsonProperty("accountAddress")
    var accountAddress : String?,
    @JsonProperty("type")
    var type : String?,
    @JsonProperty("payId")
    var payId : String?,
    @JsonProperty("notificationEndpoint")
    var notificationEndpoint : String?,
    @JsonProperty("state")
    var state : String?,
    @JsonProperty("creationDate")
    var creationDate: Date?
) : Serializable
