package com.pago.demo.myapplication.model

import java.math.BigInteger

data class Transaction(
    var id: String,
    var receiver: String,
    var sender: String,
    var amount: BigInteger,
    var currency: String
)
