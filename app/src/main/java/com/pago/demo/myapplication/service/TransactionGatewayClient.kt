package com.pago.demo.myapplication.service

import com.pago.demo.myapplication.config.TransactionGatewayConfig
import com.pago.demo.myapplication.model.Page
import com.pago.demo.myapplication.model.SignedTransactionRequest
import com.pago.demo.myapplication.model.TransactionEnvelope
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*

interface TransactionGatewayClient {
    @GET("/transactions/envelopes")
    fun getUnsignedTransactionForSender(
        @Query("sender") sender: String,
        @Query("transactionState") transactionState: String
    ): Call<Page<TransactionEnvelope>>;

    @PUT("/transactions/signed-transaction")
    @Headers("Content-Type: application/json")
    fun postSignedTransaction(@Body signedTransaction: SignedTransactionRequest): Call<Object>
}

class TransactionGatewayClientImpl {
    companion object {
        var transactionGatewayConfig: TransactionGatewayConfig = TransactionGatewayConfig()
        fun getTransactionGatewayClient(): TransactionGatewayClient {
            return Retrofit.Builder()
                .baseUrl(transactionGatewayConfig.url)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(TransactionGatewayClient::class.java)
        }
    }
}
