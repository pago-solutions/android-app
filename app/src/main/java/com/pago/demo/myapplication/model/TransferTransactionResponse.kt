package com.pago.demo.myapplication.model

import com.algorand.algosdk.transaction.SignedTransaction
import com.algorand.algosdk.transaction.Transaction
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.math.BigInteger

@JsonIgnoreProperties(ignoreUnknown = true)
data class TransferTransactionResponse(
        @JsonProperty("groupId")
        var groupId: String?,
        @JsonProperty("type")
        var type : String?,
        @JsonProperty("receiver")
        var receiver : String?,
        @JsonProperty("amount")
        var amount : BigInteger?,
        @JsonProperty("assetId")
        var assetId : String?,
        @JsonProperty("lease")
        var lease : String?,
        @JsonProperty("id")
        var id : String?,
        @JsonProperty("sender")
        var sender : String?,
        @JsonProperty("fee")
        var fee : String?,
        @JsonProperty("agreementResponse")
        var agreementResponse : AgreementResponse?,
        @JsonProperty("unsignedTransaction")
        var unsignedTransaction : Transaction?,
        @JsonProperty("signedTransaction")
        var signedTransaction : SignedTransaction?
) : Serializable, TransactionInterfaceResponse
