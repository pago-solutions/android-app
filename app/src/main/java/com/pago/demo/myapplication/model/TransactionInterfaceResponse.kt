package com.pago.demo.myapplication.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = TransferTransactionResponse::class, name = "TransferResponse"),
    JsonSubTypes.Type(value = GroupTransactionResponse::class, name = "GroupTransactionResponse")
)
interface TransactionInterfaceResponse {
}
