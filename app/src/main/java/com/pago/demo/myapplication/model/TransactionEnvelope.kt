package com.pago.demo.myapplication.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class TransactionEnvelope(
        @JsonProperty("id")
        var id : String?,
        @JsonProperty("name")
        var name : String?,
        @JsonProperty("description")
        var description : String?,
        @JsonProperty("sender")
        var sender : String?,
        @JsonProperty("applicationId")
        var applicationId : String?,
        @JsonProperty("createDate")
        var createDate : Date?,
        @JsonProperty("notificationEndpoint")
        var notificationEndpoint: String?,
        @JsonProperty("transactionState")
        var transactionState : String?,
        @JsonProperty("transactionInterfaceResponse")
        var transactionInterfaceResponse: TransactionInterfaceResponse?
) : Serializable
