package com.pago.demo.myapplication.service

import com.algorand.algosdk.account.Account
import com.algorand.algosdk.transaction.SignedTransaction
import com.pago.demo.myapplication.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransactionService() {
    var transactionGatewayClient: TransactionGatewayClient = TransactionGatewayClientImpl.getTransactionGatewayClient()

    fun getTransactionsForSender(
        sender: String,
        state: String,
        callback: (List<TransactionEnvelope>?, Throwable?) -> Unit
    ) {
        transactionGatewayClient.getUnsignedTransactionForSender(sender, state).enqueue(object : Callback<Page<TransactionEnvelope>> {
            override fun onFailure(call: Call<Page<TransactionEnvelope>>?, t: Throwable?) {
                callback(null, t)
            }

            override fun onResponse(call: Call<Page<TransactionEnvelope>>?, response: Response<Page<TransactionEnvelope>>?) {
                if (response?.body()!!.contents != null) {
                    callback(response?.body()!!.contents!!.sortedByDescending { it.createDate }, null)
                } else {
                    callback(ArrayList<TransactionEnvelope>(0), null)
                }
            }
        })
    }

    private fun filterTransactionsByState(transactionEnvelopeList: List<TransactionEnvelope>, state: String) : List<TransactionEnvelope> {
        return transactionEnvelopeList.filter { state == it.transactionState }.sortedByDescending { it.createDate }
    }

    fun signTransaction(accountMnemonic: String, transactionEnvelope: TransactionEnvelope, transactionInterface: TransferTransactionResponse, callback: (String) -> Unit) {
        var algorandAccount = Account(accountMnemonic)
        val alogorandTransaction = transactionInterface.unsignedTransaction
        val signedTransaction: SignedTransaction = algorandAccount.signTransaction(alogorandTransaction)

        var signedTransactionRequest = SignedTransactionRequest(
            transactionEnvelope.id,
            transactionInterface.id,
            signedTransaction
        )
        transactionGatewayClient.postSignedTransaction(signedTransactionRequest).enqueue(object : Callback<Object> {
            override fun onFailure(call: Call<Object>?, t: Throwable?) {
                callback("Failed")
            }

            override fun onResponse(call: Call<Object>?, response: Response<Object>?) {
                callback("Success")
            }
        })
    }
}
