package com.pago.demo.myapplication.config

data class TransactionGatewayConfig(
    var url: String = "https://transaction-gateway-dev.pagoservices.com"
)
