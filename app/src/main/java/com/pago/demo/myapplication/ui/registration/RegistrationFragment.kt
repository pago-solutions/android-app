package com.pago.demo.myapplication.ui.registration

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.iid.FirebaseInstanceId
import com.pago.demo.myapplication.MainActivity
import com.pago.demo.myapplication.R
import com.pago.demo.myapplication.model.AccountRegistrationRequest
import com.pago.demo.myapplication.model.AccountRegistrationResponse
import com.pago.demo.myapplication.service.AccountService
import com.pago.demo.myapplication.util.ParameterManager


class RegistrationFragment(private var accountService: AccountService) : Fragment() {

    private var fragmentRoot: View? = null

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            activity.currentFocus!!.windowToken, 0
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentRoot = inflater.inflate(R.layout.registration_fragment, container, false)
        fragmentRoot!!.findViewById<Button>(R.id.submitAccountButton).setOnClickListener(object : View.OnClickListener {
            override fun onClick(var1: View?) {
                hideSoftKeyboard(activity!!)
                var accountMnemonicView: Editable? = fragmentRoot!!.findViewById<TextInputEditText>(R.id.mnemonicFieldText).text
                var pagoAliasView: Editable? = fragmentRoot!!.findViewById<TextInputEditText>(R.id.pagoIdFieldText).text
                var emailAddressView: Editable? = fragmentRoot!!.findViewById<TextInputEditText>(R.id.emailAddressFieldText).text

                var accountRegistrationRequest: AccountRegistrationRequest? = AccountRegistrationRequest.buildAccountRegistration(
                    accountMnemonicView.toString(),
                    pagoAliasView.toString(),
                    emailAddressView.toString(),
                    null
                )

                val handleRegistration: (AccountRegistrationResponse?, Throwable?) -> Unit = { accountRegistrationResponse, throwable ->
                    if (throwable == null) {
                        var accountRegistrationResponse: AccountRegistrationResponse = accountRegistrationResponse!!
                        Toast.makeText(activity!!.baseContext, "${accountRegistrationResponse.payId} is set up!", Toast.LENGTH_SHORT).show()
                        ParameterManager.storeParameter(context!!, "ALGORAND_ACCOUNT_MNEMONIC", accountMnemonicView.toString())
                        ParameterManager.storeParameter(context!!, "PAGO_ALIAS", accountRegistrationResponse.payId!!)
                    } else {
                        Toast.makeText(activity!!.baseContext, "Had an error creating account: ${throwable.localizedMessage!!}", Toast.LENGTH_SHORT).show()
                    }

                    accountMnemonicView!!.clear()
                    pagoAliasView!!.clear()
                    emailAddressView!!.clear()
                    val main = activity as MainActivity?
                    main!!.getInstance()!!.openTransactionsFragment()
                }

                generateAccountAndFirebaseEndpoint(accountRegistrationRequest!!, handleRegistration)
            }
        })
        return fragmentRoot!!
    }

    fun generateAccountAndFirebaseEndpoint(accountRegistrationRequest: AccountRegistrationRequest, callback: (AccountRegistrationResponse?, Throwable?) -> Unit) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("WARN", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token : String = task.result?.token!!
                accountRegistrationRequest.notificationEndpoint = token
                accountService.createAccount(accountRegistrationRequest, callback)
            })
    }
}
