package com.pago.demo.myapplication.util

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys


class ParameterManager {
    companion object {
        fun clearParameters(activity: Context) {
            val masterKeyAlias: String = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
            val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
                "secret_shared_prefs",
                masterKeyAlias,
                activity,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
            val preferencesEditor = sharedPreferences.edit()
            preferencesEditor.clear()
            preferencesEditor.commit()
        }

        fun storeParameter(activity: Context?, parameter: String, value: String) {
            if (activity != null) {
                val masterKeyAlias: String = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

                val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
                    "secret_shared_prefs",
                    masterKeyAlias,
                    activity,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
                val preferencesEditor: SharedPreferences.Editor = sharedPreferences.edit()
                preferencesEditor.putString(parameter, value)
                preferencesEditor.commit()
            }
        }

        fun getParameter(activity: Context?, parameter: String): String? {
            if (activity != null) {
                val masterKeyAlias: String = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

                val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
                    "secret_shared_prefs",
                    masterKeyAlias,
                    activity,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
                var value: String? = sharedPreferences.getString(parameter, null)
                return value
            }
            return null
        }
    }
}