package com.pago.demo.myapplication.ui.transactions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pago.demo.myapplication.R
import com.pago.demo.myapplication.ui.transactions.model.TransactionItem
import java.lang.Exception
import java.util.*

class TransactionsViewAdapter(private val mTransactionList: ArrayList<TransactionItem>) :
    RecyclerView.Adapter<TransactionsViewAdapter.TransactionViewHolder?>() {
    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onTransactionSignClick(position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        onItemClickListener = listener
    }

    class TransactionViewHolder(itemView: View, listener: OnItemClickListener?) :
        RecyclerView.ViewHolder(itemView) {
        var transactionIconView: ImageView
        var transactionReceiverView: TextView
        var transactionCreateDateView: TextView
        var transactionAmountView: TextView
        var transactionSignView: ImageView

        init {
            transactionIconView = itemView.findViewById(R.id.transactionIcon)
            transactionReceiverView = itemView.findViewById(R.id.transactionReceiver)
            transactionCreateDateView = itemView.findViewById(R.id.transactionCreateDate)
            transactionAmountView = itemView.findViewById(R.id.transactionAmount)
            transactionSignView = itemView.findViewById(R.id.transactionSign)
            if (listener == null) {
                transactionSignView.setImageBitmap(null)
            } else {
                transactionSignView.setOnClickListener {
                    if (listener != null) {
                        val position: Int = getAdapterPosition()
                        if (position != RecyclerView.NO_POSITION) {
                            try {
                                listener.onTransactionSignClick(position)
                            } catch (e: Throwable){
                                System.out.println(e.message)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.transaction_item, parent, false)
        return TransactionViewHolder(
            v,
            onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val currentItem = mTransactionList[position]
        holder.transactionReceiverView.text = currentItem.text1
        holder.transactionCreateDateView.text = currentItem.text2
        holder.transactionAmountView.text = currentItem.text3
    }

    override fun getItemCount(): Int {
        return mTransactionList.count()
    }
}
