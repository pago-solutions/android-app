package com.pago.demo.myapplication.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Asset (
    @JsonProperty("assetId")
    var assetId: String?,
    @JsonProperty("unitName")
    var unitName: String?,
    @JsonProperty("decimals")
    var decimals: Int = 0
)
