package com.pago.demo.myapplication.ui.transactions

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.pago.demo.myapplication.service.AssetService
import com.pago.demo.myapplication.service.TransactionService

private val TAB_TITLES = arrayOf(
    "Pending",
    "History"
)

class TransactionsPagerAdapter(private val context: Context, fm: FragmentManager)
    : FragmentPagerAdapter(fm) {

    private var transactionService: TransactionService = TransactionService()
    private var assetService: AssetService = AssetService()

    override fun getItem(position: Int): Fragment {
        return TransactionsFragment(transactionService, assetService, position)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }

    override fun getCount(): Int {
        return 2
    }
}