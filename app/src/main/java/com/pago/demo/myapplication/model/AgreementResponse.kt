package com.pago.demo.myapplication.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class AgreementResponse (
        @JsonProperty("id")
        var id : String?,
        @JsonProperty("text")
        var text : String?,
        @JsonProperty("description")
        var description : String?,
        @JsonProperty("body")
        var body : String?
) : Serializable
