package com.pago.demo.myapplication.ui.account

import android.os.Bundle
import android.text.Editable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.pago.demo.myapplication.MainActivity
import com.pago.demo.myapplication.R
import com.pago.demo.myapplication.service.AccountService
import com.pago.demo.myapplication.util.ParameterManager


class AccountFragment(accountService: AccountService): Fragment() {
    private var fragmentRoot: View? = null
    private var accountDescriptionLayout: LinearLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentRoot = inflater.inflate(R.layout.account_fragment, container, false)
        accountDescriptionLayout = fragmentRoot?.findViewById(R.id.accountDescriptionLayout)
        setPayId()
        configureSignOut()
        return fragmentRoot
    }

    fun setPayId() {
        val pagoAlias = ParameterManager.getParameter(context!!, "PAGO_ALIAS")
        var payIdTextField : TextView? = fragmentRoot?.findViewById(R.id.accountPayId)
        payIdTextField?.text = pagoAlias
    }

    fun configureSignOut() {
        fragmentRoot!!.findViewById<Button>(R.id.accountSignOut).setOnClickListener(object : View.OnClickListener {
            override fun onClick(var1: View?) {
                ParameterManager.clearParameters(context!!)
                val main = activity as MainActivity?
                main!!.getInstance()!!.openTransactionsFragment()
            }
        })
    }

    fun addLineToDescription(text: String) {
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        val tv = TextView(context)
        tv.layoutParams = layoutParams
        tv.text = text
        tv.gravity = Gravity.CENTER_HORIZONTAL
        accountDescriptionLayout?.addView(tv)
    }
}
