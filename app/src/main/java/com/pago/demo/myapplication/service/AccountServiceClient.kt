package com.pago.demo.myapplication.service

import com.pago.demo.myapplication.config.TransactionGatewayConfig
import com.pago.demo.myapplication.model.AccountRegistrationRequest
import com.pago.demo.myapplication.model.AccountRegistrationResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*


interface AccountServiceClient {
    @POST("/registration/account")
    @Headers("Content-Type: application/json")
    fun createRegistration(@Body accountRegistrationRequest: AccountRegistrationRequest?): Call<AccountRegistrationResponse>
}

class AccountServiceClientImpl {
    companion object {
        var transactionGatewayConfig: TransactionGatewayConfig = TransactionGatewayConfig()
        fun getAccountServiceClient(): AccountServiceClient {
            return Retrofit.Builder()
                .baseUrl(transactionGatewayConfig.url)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(AccountServiceClient::class.java)
        }
    }
}
