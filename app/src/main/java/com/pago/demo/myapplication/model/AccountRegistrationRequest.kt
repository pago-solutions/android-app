package com.pago.demo.myapplication.model

import com.algorand.algosdk.account.Account
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.squareup.okhttp.internal.Internal
import java.io.Serializable
import java.util.logging.Level

@JsonIgnoreProperties(ignoreUnknown = true)
data class AccountRegistrationRequest(
    @JsonProperty("alias")
    var alias : String?,
    @JsonProperty("accountAddress")
    var accountAddress : String?,
    @JsonProperty("notificationEndpoint")
    var notificationEndpoint : String?,
    @JsonProperty("emailAddress")
    var emailAddress: String?,
    @JsonProperty("accountType")
    var accountType: String?

) : Serializable {
    companion object {
        fun buildAccountRegistration(
            accountMnemonic: String,
            pagoAlias: String,
            emailAddress: String,
            notificationEndpoint: String?
        ): AccountRegistrationRequest? {
            var algorandAccount: Account? = null
            try {
                algorandAccount = Account(accountMnemonic)
            } catch (e: IllegalArgumentException) {
                Internal.logger.log(Level.FINER, e.message)
                return null
            }

            return AccountRegistrationRequest(
                pagoAlias,
                algorandAccount.address.toString(),
                notificationEndpoint,
                emailAddress,
                "PERSONAL"
            )
        }
    }
}
