package com.pago.demo.myapplication.model

import com.algorand.algosdk.transaction.SignedTransaction
import com.algorand.algosdk.transaction.Transaction
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.util.*

class GroupTransactionResponse(
    @JsonProperty("groupId")
    var groupId: String?,
    @JsonProperty("type")
    var type : String?,
    @JsonProperty("id")
    var id : String?,
    @JsonProperty("sender")
    var sender : String?,
    @JsonProperty("fee")
    var fee : String?,
    @JsonProperty("agreementResponse")
    var agreementResponse : AgreementResponse?,
    @JsonProperty("unsignedTransaction")
    var unsignedTransaction : Transaction?,
    @JsonProperty("signedTransaction")
    var signedTransaction : SignedTransaction?,
    @JsonProperty("transactionInterfaceMapResponse")
    var transactionInterfaceMapResponse: MutableMap<String?, TransactionInterfaceResponse?>? = HashMap()
) : Serializable, TransactionInterfaceResponse
