package com.pago.demo.myapplication.model

import com.algorand.algosdk.transaction.SignedTransaction
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class SignedTransactionRequest(
    @JsonProperty("transactionEnvelopeId")
    var transactionEnvelopeId: String?,
    @JsonProperty("transactionInterfaceId")
    var transactionInterfaceId : String?,
    @JsonProperty("signedTransaction")
    var signedTransaction : SignedTransaction?
) : Serializable, TransactionInterfaceResponse
